==== Greg's cool timer ====

=== Usage ===
== Timekeeper / Admin mode ==
 * Timer controls are enabled
 * Time limits can be modified
 * Can be enabled by calling `makeAdmin()` in JS console.
  * Can be disabled by deleting browser cookie

== Speaker mode (Default) ==
 * No timer shown to speaker
 * Only see green/orange/red